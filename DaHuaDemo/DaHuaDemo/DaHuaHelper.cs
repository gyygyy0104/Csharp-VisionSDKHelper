﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThridLibray;
using HalconDotNet;
namespace DaHuaDemo
{
    /// <summary>
    /// 大华SDK
    /// </summary>
    class DaHuaHelper
    {
        string _DeviceName;//设备名称
        List<string> _DeviceList = new List<string>();//设备列表
        private IDevice m_dev;//设备对象
        public static HObject _Image;//采集的图片
        #region 构造函数
        public DaHuaHelper(string DeviceName)
        {
            _DeviceName = DeviceName;
        }
        #endregion

        #region 遍历设备
        /// <summary>
        /// 遍历设备
        /// </summary>
        public void EnumCamera()
        {
            _DeviceList.Clear();
            List<IDeviceInfo> li = Enumerator.EnumerateDevices();
            if (li.Count > 0)
            {
                foreach (var item in li)
                {
                    _DeviceList.Add(item.Name);
                }
            }
        }
        #endregion

        #region 初始化相机
        public bool IniCamera()
        {
            EnumCamera();
            int Index = 0;
            if (_DeviceList.Contains(_DeviceName))
            {
                Index = _DeviceList.IndexOf(_DeviceName);
            }
            else
            {
                return false;//集合中没有设备名
            }
            m_dev = Enumerator.GetDeviceByIndex(Index);
            m_dev.StreamGrabber.ImageGrabbed += OnImageGrabbed;//回调函数
            if (!m_dev.Open())
            {
                //@"连接相机失败");
                return false;
            }
            return true;
        }
        #endregion

        #region 开始采集
        public bool startGrab()
        {

            if (!m_dev.GrabUsingGrabLoopThread())
            {
               //@"开启码流失败";
                return false;
            }
            return true;
        }

        #endregion

        #region 停止采集
        public bool StopGrab()
        {
           return  m_dev.ShutdownGrab();//取消码流
        }
        #endregion

        #region 关闭相机
        public bool CloseCamera()
        {
            try
            {
                m_dev.StreamGrabber.ImageGrabbed -= OnImageGrabbed;//回调函数
                StopGrab();
                m_dev.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 回调函数
        private void OnImageGrabbed(Object sender, GrabbedEventArgs e)
        {
            // Thread.Sleep(6000);

            unsafe
            {
                fixed (byte* p = e.GrabResult.Image)
                {

                    HOperatorSet.GenImage1(out _Image, "byte", e.GrabResult.Width, e.GrabResult.Height, new IntPtr(p));
                }
            }

            //HTuple w, h;
            //HOperatorSet.GetImageSize(halcon_image, out w, out h);
            //// HOperatorSet.WriteImage(halcon_image, "bmp", 0, "d:\\1.bmp");
            //HOperatorSet.SetPart(hv_WindowHandle1, 0, 0, h - 1, w - 1);


            //HDevWindowStack.SetActive(hv_WindowHandle1);

            //if (HDevWindowStack.IsOpen())
            //{
            //    HOperatorSet.DispObj(halcon_image, hv_WindowHandle1);
            //}
            //halcon_image.Dispose();
        }

        #endregion
    }
}
