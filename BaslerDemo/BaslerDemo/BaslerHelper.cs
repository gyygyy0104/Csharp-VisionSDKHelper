﻿using Basler.Pylon;
using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// 巴斯勒SKD
    /// </summary>
    public class BaslerHelper
    {

        private string _DeviceName;//设备名称
        Dictionary<string, ICameraInfo> _DeviceList = new Dictionary<string, ICameraInfo>();//设备列表
        private IntPtr latestFrameAddress = IntPtr.Zero;
        static Version Sfnc2_0_0 = new Version(2, 0, 0);
        private Camera camera = null;//相机对象
        public HObject _Image = new HObject();//采集的图片
        int _Mode = 0;//0主动采图/1回调函数采图
        Thread InitiativeThread;//主动采图线程
        bool CameraGrabState = false;//主动采集线程状态
        #region 构造方法
        public BaslerHelper(string DeviceName, int Mode = 0)
        {
            _DeviceName = DeviceName;
            if (Mode==0|| Mode==1)
            {
                _Mode = Mode;
            }
        }
        #endregion

        #region 枚举设备
        /// <summary>
        /// 枚举设备
        /// </summary>
        /// <param name="UserID"></param>
        public void EnumCamera()
        {
            try
            {
                _DeviceList.Clear();
                // 枚举相机列表
                List<ICameraInfo> allCameraInfos = CameraFinder.Enumerate();
                foreach (ICameraInfo cameraInfo in allCameraInfos)
                {
                    _DeviceList.Add(cameraInfo[CameraInfoKey.UserDefinedName], cameraInfo);
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }
        #endregion

        #region 初始化相机
        /// <summary>
        /// 初始化相机
        /// </summary>
        public bool IniCamera()
        {
            try
            {
                EnumCamera();

                if (_DeviceList.ContainsKey(_DeviceName))
                {
                    camera = new Camera(_DeviceList[_DeviceName]);
                    if (_Mode == 0)
                    {
                        camera.StreamGrabber.ImageGrabbed -= ImageCallbackFunc;
                        camera.StreamGrabber.ImageGrabbed += ImageCallbackFunc;
                    }
                    if (camera.IsOpen) camera.Close();
                    camera.Open();
                    var imageWidth = camera.Parameters[PLCamera.Width].GetValue();               // 获取图像宽 
                    var imageHeight = camera.Parameters[PLCamera.Height].GetValue();              // 获取图像高
                    //单帧模式
                    //camera.Parameters[PLCamera.AcquisitionMode].SetValue(PLCamera.AcquisitionMode.SingleFrame);
                    //连续模式
                    camera.Parameters[PLCamera.AcquisitionMode].SetValue(PLCamera.AcquisitionMode.Continuous);
                    //NotifyG.Debug(DeviceName + "打开相机成功:" + userID);
                }
                else
                {
                    return false;//集合中没有设备名
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            return true;
        }

        #endregion

        #region 关闭相机
        /// <summary>
        /// 关闭相机
        /// </summary>
        public new bool CloseCamera()
        {
            try
            {
                StopGrab();
                camera.StreamGrabber.ImageGrabbed -= ImageCallbackFunc;
                CameraGrabState = false;
                camera.Close();
                if (InitiativeThread!=null)
                {
                    InitiativeThread.Abort();
                }
                camera.Dispose();
                if (_Image != null)
                {
                    _Image.Dispose();
                }
                if (latestFrameAddress != null)
                {
                    Marshal.FreeHGlobal(latestFrameAddress);
                    latestFrameAddress = IntPtr.Zero;
                }
                //NotifyG.Debug(DeviceName + "关闭相机成功:" + userID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            return true;
        }

        #endregion

        #region 开始采集
        /// <summary>
        /// 开始采集
        /// </summary>
        public bool StartGrab()
        {
            try
            {
                if (camera.StreamGrabber.IsGrabbing)
                {
                    //NotifyG.Error("相机当前正处于采集状态！");
                    return false;
                }
                else
                {
                    CameraGrabState = true;
                    if (_Mode == 1)
                    {
                        camera.StreamGrabber.Start(GrabStrategy.LatestImages, GrabLoop.ProvidedByStreamGrabber);
                    }
                    else
                    {
                        ImageInitiativeFunc();
                        camera.StreamGrabber.Start();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                //NotifyG.Error(DeviceName + ex.ToString());
                return false;
            }
        }
        #endregion

        #region 停止采集
        /// <summary>
        /// 停止采集
        /// </summary>
        public bool StopGrab()
        {
            try
            {
                if (camera.StreamGrabber.IsGrabbing)
                {
                    CameraGrabState = false;
                    camera.StreamGrabber.Stop();
                }
            }
            catch (Exception ex)
            {
                //NotifyG.Error(DeviceName + ex.ToString());
            }
            return true;
        }

        #endregion

        #region 回调函数
        /// <summary>
        /// 回调函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ImageCallbackFunc(Object sender, ImageGrabbedEventArgs e)
        {
            try
            {
                IGrabResult grabResult = e.GrabResult;
                if (grabResult.PixelTypeValue == PixelType.Mono8)
                {
                    if (grabResult.GrabSucceeded)
                    {
                        // Access the image data.
                        byte[] buffer = grabResult.PixelData as byte[];
                        unsafe
                        {
                            fixed (byte* p = buffer)
                            {
                                HOperatorSet.GenImage1Extern(out _Image, "byte", grabResult.Width, grabResult.Height, new IntPtr(p), 0);
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
                else if (grabResult.PixelTypeValue == PixelType.BayerBG8 || grabResult.PixelTypeValue == PixelType.BayerGB8
                    || grabResult.PixelTypeValue == PixelType.BayerRG8 || grabResult.PixelTypeValue == PixelType.YUV422packed)
                {
                    if (grabResult.GrabSucceeded)
                    {

                        byte[] buffer = grabResult.PixelData as byte[];
                        int bytesbylineGray = (grabResult.Width + 3) / 4 * 4;//灰度图像每行所占字节数
                        int bytesbylineRGB = (grabResult.Height * 3 + 3) / 4 * 4;//彩色图像每行所占字节数
                        byte[] BufferR = new byte[bytesbylineGray * grabResult.Height];
                        byte[] BufferG = new byte[bytesbylineGray * grabResult.Height];
                        byte[] BufferB = new byte[bytesbylineGray * grabResult.Height];
                        byte[] ImageBuffer = new byte[bytesbylineRGB * grabResult.Height];
                        byte[] ImageGray = new byte[bytesbylineGray * grabResult.Height];

                        for (int i = 0; i < grabResult.Height; i++)
                        {
                            for (int j = 0; j < grabResult.Width; j++)
                            {
                                BufferB[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 0];
                                BufferG[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 1];
                                BufferR[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 2];
                                // ImageGray[i * bytesbylineGray + j] = (byte)(BufferB[i * bytesbylineGray + j] * 0.11 + BufferG[i * bytesbylineGray + j] * 0.5 + BufferR[i * bytesbylineGray + j] * 0.3);
                            }
                        }
                        unsafe
                        {
                            fixed (byte* pr = BufferR, pg = BufferG, pb = BufferB)
                            {
                                HOperatorSet.GenImage3Extern(out _Image, "byte", grabResult.Width, grabResult.Height, new IntPtr(pr), new IntPtr(pg), new IntPtr(pb), 0);
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
            }
            catch (Exception)
            {

            }
       
        }
        #endregion

        #region 主动采图线程
        public void ImageInitiativeFunc()
        {
            try
            {
                InitiativeThread = new Thread(() =>
                {
                    while (CameraGrabState)
                    {
                        IGrabResult grabResult = camera.StreamGrabber.RetrieveResult(5000, TimeoutHandling.ThrowException);
                        if (grabResult.PixelTypeValue == PixelType.Mono8)
                        {
                            // Image grabbed successfully?
                            if (grabResult.GrabSucceeded)
                            {
                                //转换
                                byte[] buffer = grabResult.PixelData as byte[];
                                unsafe
                                {
                                    fixed (byte* p = buffer)
                                    {
                                        HOperatorSet.GenImage1Extern(out _Image, "byte", grabResult.Width, grabResult.Height, new IntPtr(p), 0);

                                    }
                                }
                            }
                            else
                            {
                                Thread.Sleep(10);
                            }
                        }
                        else if (grabResult.PixelTypeValue == PixelType.BayerBG8 || grabResult.PixelTypeValue == PixelType.BayerGB8
                    || grabResult.PixelTypeValue == PixelType.BayerRG8 || grabResult.PixelTypeValue == PixelType.YUV422packed)
                        {
                            if (grabResult.GrabSucceeded)
                            {

                                byte[] buffer = grabResult.PixelData as byte[];
                                int bytesbylineGray = (grabResult.Width + 3) / 4 * 4;//灰度图像每行所占字节数
                                int bytesbylineRGB = (grabResult.Height * 3 + 3) / 4 * 4;//彩色图像每行所占字节数
                                byte[] BufferR = new byte[bytesbylineGray * grabResult.Height];
                                byte[] BufferG = new byte[bytesbylineGray * grabResult.Height];
                                byte[] BufferB = new byte[bytesbylineGray * grabResult.Height];
                                byte[] ImageBuffer = new byte[bytesbylineRGB * grabResult.Height];
                                byte[] ImageGray = new byte[bytesbylineGray * grabResult.Height];

                                for (int i = 0; i < grabResult.Height; i++)
                                {
                                    for (int j = 0; j < grabResult.Width; j++)
                                    {
                                        BufferB[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 0];
                                        BufferG[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 1];
                                        BufferR[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 2];
                                        // ImageGray[i * bytesbylineGray + j] = (byte)(BufferB[i * bytesbylineGray + j] * 0.11 + BufferG[i * bytesbylineGray + j] * 0.5 + BufferR[i * bytesbylineGray + j] * 0.3);
                                    }
                                }
                                unsafe
                                {
                                    fixed (byte* pr = BufferR, pg = BufferG, pb = BufferB)
                                    {
                                        HOperatorSet.GenImage3Extern(out _Image, "byte", grabResult.Width, grabResult.Height, new IntPtr(pr), new IntPtr(pg), new IntPtr(pb), 0);
                                    }
                                }
                            }
                            else
                            {
                                Thread.Sleep(10);
                            }
                        }
                    }
                });
                InitiativeThread.IsBackground = true;
                InitiativeThread.Start();
            }
            catch (Exception)
            {
            }

        }
        #endregion

    }
}