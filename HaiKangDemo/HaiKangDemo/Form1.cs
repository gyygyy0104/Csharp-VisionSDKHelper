﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HalconDotNet;

namespace HaiKangDemo
{
    public partial class Form1 : Form
    {
        MVCameraHelper mVCameraHelper = new MVCameraHelper("7C01A2DPAK00019");
        public Form1()
        {
            InitializeComponent();
            DisPlaySet();
            mVCameraHelper.OpenCamera();
            
        }
        public void DisPlaySet()
        {
            HTuple hv_WindowHandle;
            HOperatorSet.OpenWindow(0, 0, pict_ImageShow.Width, pict_ImageShow.Height, pict_ImageShow.Handle, "visible", "", out hv_WindowHandle);
            HDevWindowStack.Push(hv_WindowHandle);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            mVCameraHelper.StopGrab();
            mVCameraHelper.ColseCamera();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mVCameraHelper.startGrab();
        }
    }
}
