﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MVSDK;//使用SDK接口
using CameraHandle = System.Int32;
using MvApi = MVSDK.MvApi;
using HalconDotNet;

namespace MaiDeWeiShiDemo
{
    /// <summary>
    /// 迈德微视SDK
    /// </summary>
    class MaiDeWeiShiHelper
    {
        tSdkCameraDevInfo[] tCameraDevInfoList;//设备列表(SDK)
        string _DeviceName;//设备名称
        List<string> _DeviceList = new List<string>();//设备列表
        CameraHandle m_hCamera = 0;//设备对象句柄
        tSdkCameraCapbility tCameraCapability;
        IntPtr m_ImageBuffer;
        CAMERA_SNAP_PROC m_CaptureCallback;//回调函数
        IntPtr m_iCaptureCallbackCtx;     //图像回调函数的上下文参数
        CAMERA_SNAP_PROC pCaptureCallOld = null;
        int _Mode = 0;//0主动采图/1回调采图
        bool CameraGrabState = false;
        Thread InitiativeThread;//主动采图线程
        public static HObject _Image;//采集的图片
        #region 构造方法
        public MaiDeWeiShiHelper(string DeviceName, int Mode = 0)
        {
            _DeviceName = DeviceName;
            if (Mode == 0 || Mode == 1)
            {
                _Mode = Mode;
            }

        }
        #endregion
        #region 枚举设备
        public void EnumCamera()
        {
            //1查找（枚举）设备
            CameraSdkStatus status = MvApi.CameraEnumerateDevice(out tCameraDevInfoList);
            if (status == CameraSdkStatus.CAMERA_STATUS_SUCCESS)
            {
                foreach (var item in tCameraDevInfoList)
                {
                    _DeviceList.Add(item.acSn.ToString());
                }
            }
        }
        #endregion

        #region 初始化设备
        public bool OpenCamera()
        {
            try
            {
                EnumCamera();
                int Index = 0;
                if (_DeviceList.Contains(_DeviceName))
                {
                    Index = _DeviceList.IndexOf(_DeviceName);
                }
                else
                {
                    return false;//集合中没有设备名
                }
                if (tCameraDevInfoList != null)//此时iCameraCounts返回了实际连接的相机个数。如果大于1，则初始化第一个相机
                {
                    //2初始化相机
                    CameraSdkStatus status = MvApi.CameraInit(ref tCameraDevInfoList[Index], -1, -1, ref m_hCamera);
                    //获取相机描述
                    MvApi.CameraGetCapability(m_hCamera, out tCameraCapability);
                    //申请内存
                    m_ImageBuffer = Marshal.AllocHGlobal(tCameraCapability.sResolutionRange.iWidthMax * tCameraCapability.sResolutionRange.iHeightMax * 3 + 1024);

                    if (tCameraCapability.sIspCapacity.bMonoSensor != 0)
                    {
                        // 黑白相机输出8位灰度数据
                        MvApi.CameraSetIspOutFormat(m_hCamera, (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8);
                    }
                    if (_Mode == 1)
                    {
                        //注册回调函数
                        m_CaptureCallback = new CAMERA_SNAP_PROC(ImageCaptureCallback);
                        MvApi.CameraSetCallbackFunction(m_hCamera, m_CaptureCallback, m_iCaptureCallbackCtx, ref pCaptureCallOld);
                    }


                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion

        #region 开始采集
        public bool StartGrab()
        {
            try
            {
                if (m_hCamera < 1)//还未初始化相机
                {
                    return false;
                }
                if (_Mode == 1)
                {
                    MvApi.CameraPlay(m_hCamera);
                }
                else
                {
                    if (!InitiativeThread.IsAlive)
                    {
                        if (!InitiativeThread.IsAlive)
                        {
                            CameraGrabState = true;
                            ImageInitiativeFunc();
                            MvApi.CameraPlay(m_hCamera);
                            
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region 停止采集
        public bool StopGrab()
        {
            try
            {
                CameraGrabState = false;
                MvApi.CameraPause(m_hCamera);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
        #endregion

        #region 关闭相机
        public bool CloseCamera()
        {
            try
            {
                StopGrab();
                if (InitiativeThread != null)
                {
                    InitiativeThread.Abort();//主动采集线程释放
                }
                MvApi.CameraUnInit(m_hCamera);
                Marshal.FreeHGlobal(m_ImageBuffer);
                m_hCamera = 0;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion


        #region 回调函数
        public void ImageCaptureCallback(CameraHandle hCamera, IntPtr pFrameBuffer, ref tSdkFrameHead pFrameHead, IntPtr pContext)
        {
            try
            {
                if (pFrameHead.uiMediaType == (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8)
                {
                    MvApi.CameraImageProcess(hCamera, pFrameBuffer, m_ImageBuffer, ref pFrameHead);
                    int bytesbylineGray = (pFrameHead.iWidth + 3) / 4 * 4;//灰度图像每行所占字节数
                    byte[] ImageGray = new byte[bytesbylineGray * pFrameHead.iHeight];
                    Marshal.Copy(m_ImageBuffer, ImageGray, 0, bytesbylineGray * pFrameHead.iHeight);

                    unsafe
                    {
                        fixed (byte* p = ImageGray)
                        {
                            HOperatorSet.GenImage1Extern(out _Image, "byte", pFrameHead.iWidth, pFrameHead.iHeight, new IntPtr(p), 0);
                        }
                    }
                }
                else if (pFrameHead.uiMediaType == (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_BGR8)
                {
                    int bytesbylineGray = (pFrameHead.iWidth + 3) / 4 * 4;//灰度图像每行所占字节数
                    int bytesbylineRGB = (pFrameHead.iHeight * 3 + 3) / 4 * 4;//彩色图像每行所占字节数
                    byte[] BufferR = new byte[bytesbylineGray * pFrameHead.iHeight];
                    byte[] BufferG = new byte[bytesbylineGray * pFrameHead.iHeight];
                    byte[] BufferB = new byte[bytesbylineGray * pFrameHead.iHeight];
                    byte[] ImageBuffer = new byte[bytesbylineRGB * pFrameHead.iHeight];
                    byte[] ImageGray = new byte[bytesbylineGray * pFrameHead.iHeight];

                    for (int i = 0; i < pFrameHead.iHeight; i++)
                    {
                        for (int j = 0; j < pFrameHead.iWidth; j++)
                        {
                            BufferB[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 0];
                            BufferG[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 1];
                            BufferR[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 2];
                            //三通转单通道
                            // ImageGray[i * bytesbylineGray + j] = (byte)(BufferB[i * bytesbylineGray + j] * 0.11 + BufferG[i * bytesbylineGray + j] * 0.5 + BufferR[i * bytesbylineGray + j] * 0.3);
                        }
                    }
                    unsafe
                    {
                        fixed (byte* pr = BufferR, pg = BufferG, pb = BufferB)
                        {
                            HOperatorSet.GenImage3Extern(out _Image, "byte", pFrameHead.iWidth, pFrameHead.iHeight, new IntPtr(pr), new IntPtr(pg), new IntPtr(pb), 0);
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
   
        }

        #endregion
        #region 主动采图线程
        public void ImageInitiativeFunc()
        {
            try
            {
                CameraSdkStatus eStatus;
                tSdkFrameHead FrameHead;
                IntPtr uRawBuffer;//rawbuffer由SDK内部申请。应用层不要调用delete之类的释放函数
                InitiativeThread = new Thread(() =>
                {
                    while (CameraGrabState)
                    {
                        eStatus = MvApi.CameraGetImageBuffer(m_hCamera, out FrameHead, out uRawBuffer, 500);
                        if (eStatus == CameraSdkStatus.CAMERA_STATUS_SUCCESS)//如果是触发模式，则有可能超时
                        {
                            if (FrameHead.uiMediaType == (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8)
                            {
                                MvApi.CameraImageProcess(m_hCamera, uRawBuffer, m_ImageBuffer, ref FrameHead);
                                int bytesbylineGray = (FrameHead.iWidth + 3) / 4 * 4;//灰度图像每行所占字节数
                                byte[] ImageGray = new byte[bytesbylineGray * FrameHead.iHeight];
                                Marshal.Copy(m_ImageBuffer, ImageGray, 0, bytesbylineGray * FrameHead.iHeight);

                                unsafe
                                {
                                    fixed (byte* p = ImageGray)
                                    {
                                        HOperatorSet.GenImage1Extern(out _Image, "byte", FrameHead.iWidth, FrameHead.iHeight, new IntPtr(p), 0);
                                    }
                                }
                            }
                            else if (FrameHead.uiMediaType == (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_BGR8)
                            {
                                int bytesbylineGray = (FrameHead.iWidth + 3) / 4 * 4;//灰度图像每行所占字节数
                                int bytesbylineRGB = (FrameHead.iHeight * 3 + 3) / 4 * 4;//彩色图像每行所占字节数
                                byte[] BufferR = new byte[bytesbylineGray * FrameHead.iHeight];
                                byte[] BufferG = new byte[bytesbylineGray * FrameHead.iHeight];
                                byte[] BufferB = new byte[bytesbylineGray * FrameHead.iHeight];
                                byte[] ImageBuffer = new byte[bytesbylineRGB * FrameHead.iHeight];
                                byte[] ImageGray = new byte[bytesbylineGray * FrameHead.iHeight];

                                for (int i = 0; i < FrameHead.iHeight; i++)
                                {
                                    for (int j = 0; j < FrameHead.iWidth; j++)
                                    {
                                        BufferB[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 0];
                                        BufferG[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 1];
                                        BufferR[i * bytesbylineGray + j] = ImageBuffer[i * bytesbylineRGB + j * 3 + 2];
                                        //三通转单通道
                                        // ImageGray[i * bytesbylineGray + j] = (byte)(BufferB[i * bytesbylineGray + j] * 0.11 + BufferG[i * bytesbylineGray + j] * 0.5 + BufferR[i * bytesbylineGray + j] * 0.3);
                                    }
                                }
                                unsafe
                                {
                                    fixed (byte* pr = BufferR, pg = BufferG, pb = BufferB)
                                    {
                                        HOperatorSet.GenImage3Extern(out _Image, "byte", FrameHead.iWidth, FrameHead.iHeight, new IntPtr(pr), new IntPtr(pg), new IntPtr(pb), 0);
                                    }
                                }
                            }
                        }
                        //成功调用CameraGetImageBuffer后必须释放，下次才能继续调用CameraGetImageBuffer捕获图像。
                        MvApi.CameraReleaseImageBuffer(m_hCamera, uRawBuffer);
                    }
                })
                {
                    IsBackground = true,
                };
                InitiativeThread.Start();
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion
    }
}
